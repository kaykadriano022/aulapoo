package kayk.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AulaDia08Application {

	public static void main(String[] args) {
		SpringApplication.run(AulaDia08Application.class, args);
	}

}
